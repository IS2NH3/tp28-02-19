<?php

function getDinos()
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getoneDinos($slug)
{
    $response = Requests::get("https://medusa.delahayeyourself.info/api/dinosaurs/$slug");
    return json_decode($response->body);
}